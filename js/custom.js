/**
 * Custom JS
 * Use this file to add your custom scripts
 */
/**
 * Theme JS
 */

$(function () {
    $(".section_question_and_answer td").click(function () {
        var qa_title = $(this).find(".qa_title").html();
        var context = $(this).find(".hidden").html();
        $('#qa_modal').find(".modal-title").html(qa_title);
        $('#qa_modal').find(".modal-body").html(context);
        $('#qa_modal').modal()
    })
});

$(function () {
  $("form#form_sendemail select#inquiry").change(function () {
    $("form#form_sendemail select,form#form_sendemail input,form#form_sendemail textarea").map(function (idx, ele) {
      $(ele).closest('div.form-group').show();
    });
    const selVal = $(this).val();
    if (selVal === 'ＱＲホテル導入') {
      
    } else if (selVal === '資料請求') {
      $("form#form_sendemail #lodging").closest('div.form-group').hide();
      $("form#form_sendemail #room").closest('div.form-group').hide();
      $("form#form_sendemail #message").closest('div.form-group').hide();
    } else if (selVal === 'メディア取材' || selVal === 'その他' || selVal === '') {
      $("form#form_sendemail #lodging").closest('div.form-group').hide();
      $("form#form_sendemail #room").closest('div.form-group').hide();
    }
  })
});