/**
 * Contact form
 */
var n = 0;
var numberStringList = ["①", "②", "③", "④", "⑤", "⑥", "⑦", "⑧", "⑨", "⑩"];

function addfacility() {
    var myitem = $($("#facility-temp").html()).clone();
    myitem.find("#addnumber").text(numberStringList[n]);
    $("#facilities").append(myitem);
    n++;
    if (n == 10) {
        $("#button_add").hide();
    }
}

function getApplicationMessage() {
    var message = "<html>" +
    '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">' +
    '  <body>'
    + '<p style="font-weight: 400;">この度は、中国観光客向けインバウンドサービス「QRHOTEL」の</p>'
    +'<p style="font-weight: 400;">無料トライアルをお申込みいただき、誠にありがとうございます。<br /><br />弊社担当者から後ほど、頂戴した内容に関するご連絡を差し上げますので、<br />今しばらくお待ちいただけますと幸いでございます。<br /><br />引き続き、何卒、よろしくお願い申し上げます。<br /><br /></p>'
    +'<p>※このメッセージは、ホームページにてお問い合わせいただいた方へ送信しております。<br />弊社からご案内を差し上げる場合がありますのでご了承ください。<br /><br /></p>'
    +'<p>********************************************&nbsp;&nbsp;記&nbsp;*****************************************</p>'
    +'<p>■サービス名称</p>'
    +'<p>　　QRHOTEL</p>'
    +'<p><br />■サービス概要・ねらい</p>'
    +'<p>・中国人宿泊客とのコミュニケーションの壁を乗り越える中国のスーパーアプリ（WeChat）の活用により、</p>'
    +'<p>&nbsp; &nbsp; ホテルや旅館における中国人観光客の積極的な取込みによる売上拡大を支援します。</p>'
    +'<p>・中国人宿泊客にコミュニケーション不足のない快適なホテルライフを過ごしていただく事により、<br />　リピーターや、紹介による新規顧客の拡大を図ります。</p>'
    +'<p><br />■サービスの特徴</p>'
    +'<p>　①中国人のお客様に特化</p>'
    +'<p>　②お客様が持っているスマホで稼働するアプリを提供</p>'
    +'<p><br />■サービス内容</p>'
    +'<p>　①中国人のお客様に対し、コンシェルジュ・サービスを中心とした案内サービスを行います。　</p>'
    +'<p>➡高度な案内機能による「おもてなし」の実現</p>'
    +'<p>□具体的なサービス（抜粋）</p>'
    +'<p>　　・日中音声通訳</p>'
    +'<p>　　・施設案内</p>'
    +'<p>　　・館内レストランの案内</p>'
    +'<p>　　・周辺のコンビニやドラッグストアの案内</p>'
    +'<p>　　・帰館経路案内</p>'
    +'<p>・Wi-Fi接続誘導</p>'
    +'<p>　②中国人のお客様に対し、便利な予約サービスを提供します。</p>'
    +'<p>➡便利な予約機能による「売上拡大」の実現</p>'
    +'<p>□具体的なサービス（抜粋）</p>'
    +'<p>　　・宿泊予約</p>'
    +'<p>　　・館内商品販売</p>'
    +'<p>　　・周辺レストラン予約　等<br /><br /></p>'
    +'<p>***********問い合わせ先******************************************************</p>'
    +'<p>株式会社　イー・ビジネス</p>'
    +'<p>〒105-0014　東京都港区芝2-28-8芝2丁目ビル10階</p>'
    +'<p>TEL ：03-6809-3235　FAX　03-6809-3238<br />ホームページ ：&nbsp;&nbsp;<a href="http://www.e-business.co.jp/">www.e-business.co.jp</a>&nbsp;&nbsp;</p>'
    +'<p>E-mail： qrhotel<a href="mailto:yuyang@e-business.co.jp">@e-business.co.jp</a><br />***************************************************************************&nbsp;</p>'
    +'</body></html>';
    return message;
}

$(document).ready(function (e) {

    addfacility();
    $("#button_add").click(addfacility);

    $("#modal-submit").click(function () {
        $('#largeModal').modal('hide');
        $('#form_sendemail').submit();
    });

    //validate
    $('#submit').click(function (e) {
        e.isDefaultPrevented();
        $("#submit").addClass("disabled").attr({
            "disabled": "disabled"
        })
        $('#form_sendemail .has-error').removeClass('has-error');
        var hasError = false;
        var email = $('#form_sendemail').find("#email").val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(email).toLowerCase())) {
            $('#form_sendemail').find('#email').parent().addClass('has-error');
            $('#form_sendemail #email').next('.help-block').html('正しいメールアドレスを記入ください。').show();
            $('#form_sendemail').find('#email').focus();
            hasError = true;
        }else{
            $('#form_sendemail #email').next('.help-block').html('正しいメールアドレスを記入ください。').hide();
        }

        var name = $("#name").val();
        if (!name) {
            $('#form_sendemail').find('#name').parent().addClass('has-error');
            $('#form_sendemail #name').next('.help-block').html('担当者名を記入ください。').show();
            $('#form_sendemail').find('#name').focus();
            hasError = true;
        }else{
            $('#form_sendemail #name').next('.help-block').html('担当者名を記入ください。').hide();
        }

        // キャンセルでも活性化できるようにする。
        $("#submit").removeClass("disabled").attr({
            "disabled": false
        })

        if (hasError) {
            return false;
        }
        return true;
    });
    $('#form_sendemail').submit(function (e) {
        // if the validator does not prevent form submit
        e.isDefaultPrevented();
        $('#form_sendemail .has-error').removeClass('has-error');
        $('div.alert').removeClass('alert-success').html('');

        var companyName = $("#companyName").val();
        var companyKN = $("#companyKN").val();
        var postcode = $("#postcode").val();
        var residence = $("#residence").val();
        var tel = $("#tel").val();
        var fax = $("#FAX").val();

        var name = $("#name").val();
        var nameKN = $("#nameKN").val();
        var email = $("#email").val();
        var facilities = '<h3>トレイアル施設情報</h3>';
        $("#facilities").children().each(function () {
            var No = $(this).find("#addnumber").val();
            var facilityname = $(this).find("#facilityname").val();
            var room = $(this).find("#room").val();
            facilities += '<p><pre class="title">施設名' + No + '：</pre><pre>' + facilityname + '</pre></p><hr />';
            facilities += '<p><pre class="title">客室数' + No + '：</pre><pre>' + room + '</pre></p><hr />';
        });
        var message = "<html>" +
            '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">' +
            '  <style type="text/css">' +
            '    pre {font-size: 13px;}' +
            '    pre.title {padding-top: 5px; padding-bottom: 5px; border-bottom: 1px dashed lightgray;}' +
            '  </style>' +
            "<body>" +
            "<p><pre class='title'>会社名：</pre><pre>" + companyName + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>会社名カナ：</pre><pre>" + companyKN + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>郵便番号：</pre><pre>" + postcode + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>住所：</pre><pre>" + residence + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>電話番号：</pre><pre>" + (tel ? tel : 'なし') + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>FAX：</pre><pre>" + (fax ? fax : 'なし') + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>担当者名：</pre><pre>" + name + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>担当者名カナ：</pre><pre>" + nameKN + "</pre></p>" +
            '<hr />' +
            "<p><pre class='title'>メールアドレス：</pre><pre>" + email + "</pre></p>" +
            '<hr />' +
            facilities +
            "</body>" +
            "</html>";
        grecaptcha.ready(function () {
            grecaptcha.execute('6LcJsZ4UAAAAAIx-LtyKLzUkc_IL6UT3A-SUJ71z', {
                action: 'homepage'
            }).then(function (token) {
                var data = {
                    "name": name,
                    "email": email,
                    "message": message,
                    "selfmessage": getApplicationMessage(),
                    "token": token
                };
                var jsonData = JSON.stringify(data);
                var url =
                    "https://g4ksq0izwa.execute-api.ap-northeast-1.amazonaws.com/v1/sendmail";

                $.ajax({
                    method: "POST",
                    url: url,
                    data: jsonData
                }).done(function (msg) {
                    $("#submit").removeClass("disabled").attr({
                        "disabled": false
                    })
                    $('div.alert').addClass('alert-success').html("送信しました").slideDown();
                    $('#form_sendemail')[0].reset();
                }).fail(function (jqXHR, textStatus) {
                    $("#submit").removeClass("disabled").attr({
                        "disabled": false
                    })
                    $('#form_sendemail')[0].reset();
                });
            });
        });
        return false;
    });
});
