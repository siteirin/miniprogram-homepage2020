Chart.defaults.global.title.fontSize = 19;
Chart.defaults.global.title.fontFamily = "'Oswald', sans-serif";
Chart.defaults.global.title.fontStyle = '400';
Chart.defaults.global.title.fontColor = '#353535';

new Chart(document.getElementById("canvas"), {
    "type": "bar",
    "data": {
        "labels": ["レビュー・評価", "観光地の配信情報", "値段・価格", "知合いの口コミ", "ポイントサービス", "有益な広告", ],
        "datasets": [{
            "data": [92.0, 91.0, 88.0, 88.0, 82.0, 76.0],
            "fill": false,
            "backgroundColor": [
                "rgba(255, 99, 132, 0.2)",
                "rgba(255, 159, 64, 0.2)",
                "rgba(255, 205, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(153, 102, 255, 0.2)"
            ],
            "borderColor": [
                "rgb(255, 99, 132)",
                "rgb(255, 159, 64)",
                "rgb(255, 205, 86)",
                "rgb(75, 192, 192)",
                "rgb(54, 162, 235)",
                "rgb(153, 102, 255)",
                "rgb(201, 203, 207)"
            ],
            "borderWidth": 1
        }]
    },
    "options": {
        "title": {
            "display": true,
            "text": '中国人が旅行先を選ぶには影響されやすいポイント'
        },
        "legend": {
            "display": false
        },
        "scales": {
            "yAxes": [{
                "ticks": {
                    "beginAtZero": true
                }
            }]
        }
    }
});